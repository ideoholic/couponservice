package org.ideoholic.storediscount.service;


import org.ideoholic.storediscount.dao.DiscountDataDao;
import org.ideoholic.storediscount.db.StoreDiscount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class DiscountServiceImpl implements DiscountService {
	
	private final static Logger logger = LoggerFactory.getLogger(DiscountServiceImpl.class);
	
	@Autowired
	private DiscountDataDao discountDataDao;
	
	
    public Iterable < StoreDiscount > getAllProducts() {
        return discountDataDao.findAll();
    }
    
    public StoreDiscount getProduct(long id) {
        return discountDataDao
          .findById(id);
         // .orElseThrow(() ->  new ResourceNotFoundException("Id not found"));
    }
	
    public String update(StoreDiscount storeDiscount) {
        this.discountDataDao.save(storeDiscount);
    }
	
    
    public String delete(long productId) {
		discountDataDao.deleteById(productId);
		return null;
    }
	
	}
