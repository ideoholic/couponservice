package org.ideoholic.storediscount.service;

import org.ideoholic.storediscount.db.StoreDiscount;

public interface DiscountService {

	Iterable<StoreDiscount> getAllProducts();

	String update(StoreDiscount storeDiscount);
	
	String delete(long productId);

	

	

}
