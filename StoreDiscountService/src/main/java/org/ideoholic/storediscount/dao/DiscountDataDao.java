package org.ideoholic.storediscount.dao;

import org.ideoholic.storediscount.db.StoreDiscount;
import org.springframework.data.repository.CrudRepository;

public interface DiscountDataDao extends CrudRepository<StoreDiscount, Long>
{

}
