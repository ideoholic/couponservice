package org.ideoholic.storediscount;

import javax.validation.constraints.NotNull;

import org.ideoholic.storediscount.db.StoreDiscount;
import org.ideoholic.storediscount.dto.DiscountProfileDto;
import org.ideoholic.storediscount.service.DiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DiscountRestApiResource {
	
	@Autowired
	private DiscountService discountService;
	
	@GetMapping(value = { "", "/" })
    public @NotNull Iterable<StoreDiscount> getProducts() {
        return discountService.getAllProducts();
    }

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseEntity Update(final DiscountProfileDto discountProfileDto ){
		String output = "";
		output =discountService.update(null);
		return new ResponseEntity("output", HttpStatus.OK);

	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public ResponseEntity Delete(final DiscountProfileDto discountProfileDto ){
		String output = "";
		output =discountService.delete(discountProfileDto.getProductId());
		return new ResponseEntity("output", HttpStatus.OK);

	}
}
