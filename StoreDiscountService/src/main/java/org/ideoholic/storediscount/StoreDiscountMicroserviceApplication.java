package org.ideoholic.storediscount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@EnableEurekaClient
@EnableDiscoveryClient
@SpringBootApplication
public class StoreDiscountMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StoreDiscountMicroserviceApplication.class, args);
	}
	
}
