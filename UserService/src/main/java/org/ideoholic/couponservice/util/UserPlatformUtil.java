package org.ideoholic.couponservice.util;

import java.util.Date;

import org.ideoholic.couponservice.userservice.data.db.User;
import org.ideoholic.couponservice.userservice.data.dto.UserDataDto;

public class UserPlatformUtil {

	public static Date getCurrentDate() {
		Date date=java.util.Calendar.getInstance().getTime();  
		return date;
	}

	public static User generateUserFromUserData(UserDataDto userData) {

		User user = new User();
		user.setUserName(userData.getUserName());
		user.setPassword(userData.getPassword());
		user.setIsActive(userData.isActive());
		user.setRegistrationDate(userData.getRegistrationDate());
		user.setDeactivationDate(userData.getDeactivationDate());
		user.setReactivationDate(userData.getReactivationDate());
		return user;
		
	}

	public static UserDataDto generateUserDataFromUser(User user) {
		UserDataDto data = UserDataDto.instance(user.getRowId(), user.getUserName(), user.getPassword(), user.getIsActive(), user.getRegistrationDate(), user.getDeactivationDate(), user.getReactivationDate());
		return data;
	}



}
