package org.ideoholic.couponservice.util;

import java.util.Random;

import org.ideoholic.couponservice.userservice.constants.UserServiceConstants;

public class RowIdGenerator implements UserServiceConstants{

	  public static String generateRowId(int length) {

			// ||, -, *, /, <>, <, >, ,(comma), =, <=, >=, ~=, !=, ^=, (, )
			int len = length / 2;
			String letters = new String("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
			String numbers = new String("1234567890");
			int lettersLength = letters.length();
			int numbersLength = numbers.length();

			StringBuffer generatedRandomString = new StringBuffer();

			for (int i = 0; i < len; i++) {
				int index = generateIndex(lettersLength);
				generatedRandomString.append(letters.charAt(index));
			}

			for (int i = 0; i < len; i++) {
				int index = generateIndex(numbersLength);
				generatedRandomString.append(numbers.charAt(index));
			}
			if (generatedRandomString.length() >= length) {
				generatedRandomString = generatedRandomString.delete(length - 1, generatedRandomString.length());
			}
			generatedRandomString = generatedRandomString.append(SERVICE_IDENTIFIER);
			return generatedRandomString.toString();

		}

	   private static int generateIndex(int stringLength) {
	       int newRandomNumber = returnRandom(stringLength);
	       return newRandomNumber;
	   }

	   private static int returnRandom(int stringLength) {
	       Random random = new Random();
	       int mathrandom = random.nextInt(59245);

	       do {
	           double randomDouble = Math.random();
	           if (randomDouble < 0.01) continue;
	           mathrandom = (int) (mathrandom * randomDouble);
	       } while (mathrandom >= stringLength);
	       return mathrandom;
	   }

}
