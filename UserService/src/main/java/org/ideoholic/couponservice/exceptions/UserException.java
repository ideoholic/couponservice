package org.ideoholic.couponservice.exceptions;

import org.ideoholic.couponservice.userservice.data.dto.UserDataDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserException extends Exception {
	public enum ErrorType {
		USER_NAME_ALREADY_EXISTS, USER_UPDATATION_FAILED, USER_DEACTIVATION_FAILED, USER_DOESNOT_EXISTS, USERS_DOESNOT_EXISTS, USER_ACTIVATION_FAILED;
	}

	private ErrorType type;
	private UserDataDto userDto;

	public UserException(UserDataDto userDto, ErrorType type) {
		super("User "+userDto.getUserName());
		this.type = type;
		this.userDto = userDto;
	}

	public ErrorType getType() {
		return type;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(this.getMessage());
		switch (type) {
		case USER_NAME_ALREADY_EXISTS:
			sb.append("already exists!");
			break;
		case USER_UPDATATION_FAILED:
			sb.append("updation failed");
			break;
		case USER_DEACTIVATION_FAILED:
			sb.append("deactivation failed");
			break;
		case USER_DOESNOT_EXISTS:
			sb.append("doesn't exists");
			break;	
		case USERS_DOESNOT_EXISTS:
			sb.append("doesn't exists");
			break;	
		case USER_ACTIVATION_FAILED:
			sb.append("activation failed");
			break;
		default:
			break;
		}
		return sb.toString();
	}

}
