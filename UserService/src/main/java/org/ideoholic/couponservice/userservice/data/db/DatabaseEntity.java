package org.ideoholic.couponservice.userservice.data.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.ideoholic.couponservice.userservice.constants.UserServiceConstants;
import org.ideoholic.couponservice.util.RowIdGenerator;
import org.springframework.data.domain.Persistable;

@MappedSuperclass
public abstract class DatabaseEntity<PK extends Serializable> implements Persistable<Long>, UserServiceConstants {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public boolean isNew() {
		return null == this.id;
	}

	@Column(name = "row_id", unique = true)
	protected final String rowId = RowIdGenerator.generateRowId(ROWID_LENGTH);

	public String getRowId() {
		return rowId;
	}
}