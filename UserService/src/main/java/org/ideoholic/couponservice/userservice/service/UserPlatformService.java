
package org.ideoholic.couponservice.userservice.service;

import java.util.Collection;

import org.ideoholic.couponservice.exceptions.UserException;
import org.ideoholic.couponservice.userservice.data.dto.UserDataDto;

public interface UserPlatformService {

	UserDataDto validateAndSaveUser(UserDataDto userDataDto) throws UserException;

	UserDataDto getUserByrowId(String rowId) throws UserException;

	Collection<UserDataDto> getAllUsersData() throws UserException;

	UserDataDto deactivateUserByRowId(String rowId) throws UserException;

	UserDataDto updateUserByRowId(UserDataDto userDataDto, String rowId) throws UserException;

	int deactivateUsers();

	UserDataDto reactivateUserByRowId(UserDataDto userDataDto, String rowId) throws UserException;

}
