package org.ideoholic.couponservice.userservice.data.dao;

import java.util.Collection;

import javax.transaction.Transactional;

import org.ideoholic.couponservice.userservice.data.db.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDataDao extends JpaRepository<User, Long> {

	@Query("select u from User u where u.userName = ?1")
	User findByUserName(String userName);

	@Query("select u from User u where u.rowId = ?1")
	User getUserByrowId(String rowId);

	@Query("SELECT u FROM User u where u.isActive = true")
	Collection<User> getAllUsersData();

	@Transactional
	@Modifying
	@Query("update User u set u.userName= ?1, u.password = ?2 where rowId = ?3")
	int updateUser(String userName, String password, String rowId);

	@Transactional
	@Modifying
	@Query("update User u set u.isActive = false where u.rowId = ?1")
	int deactivateUserByRowId(String rowId);

	@Transactional
	@Modifying
	@Query("update User u set u.isActive = false")
	int deactivateAllUsers();

	@Transactional
	@Modifying
	@Query("update User u set u.isActive = true where u.rowId = ?1")
	int reactivateUser(String rowId);

}
