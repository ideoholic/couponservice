package org.ideoholic.couponservice.userservice.service;

import java.util.ArrayList;
import java.util.Collection;

import org.ideoholic.couponservice.exceptions.UserException;
import org.ideoholic.couponservice.userservice.data.dao.UserDataDao;
import org.ideoholic.couponservice.userservice.data.db.User;
import org.ideoholic.couponservice.userservice.data.dto.UserDataDto;
import org.ideoholic.couponservice.util.UserPlatformUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserPlatformServiceImpl implements UserPlatformService {

	private final static Logger logger = LoggerFactory.getLogger(UserPlatformServiceImpl.class);
	private final UserDataDao userDao;

	@Autowired
	public UserPlatformServiceImpl(UserDataDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public UserDataDto validateAndSaveUser(UserDataDto userDataDto) throws UserException {
		
		logger.debug("UserPlatformServiceImpl.createUser: user data being saved:" + userDataDto);
		
		User user = UserPlatformUtil.generateUserFromUserData(userDataDto);
		User userDetails = userDao.findByUserName(user.getUserName());
		
			if(userDetails  == null) {
				userDao.saveAndFlush(user);
			}else {
				throw new UserException(userDataDto,UserException.ErrorType.USER_NAME_ALREADY_EXISTS);
			}
		
		userDataDto = UserDataDto.instance(user.getRowId(),user.getUserName(), user.getPassword(), true, user.getRegistrationDate(), user.getDeactivationDate(), user.getReactivationDate());
		return userDataDto;
		
	}

	@Override
	public UserDataDto getUserByrowId(String rowId) throws UserException {
		logger.debug("UserPlatformServiceImpl.User: get user data:" + rowId);
		User user = userDao.getUserByrowId(rowId);
		UserDataDto userDto = UserDataDto.lookup(rowId);
			if(user == null) {
				throw new UserException(userDto,UserException.ErrorType.USER_DOESNOT_EXISTS);
			}
		UserDataDto userDataDto = UserPlatformUtil.generateUserDataFromUser(user);
		logger.debug("UserPlatformServiceImpl.getUserByRowId:" + userDataDto.getUserName());
		return userDataDto;
	}

	@Override
	public Collection<UserDataDto> getAllUsersData() throws UserException {
		logger.debug("UserPlatformServiceImpl.All Users Data");
		Collection<User> userList = userDao.getAllUsersData();
		Collection<UserDataDto> userDataDtoList = new ArrayList<UserDataDto>();
		
		if(userList !=null) {
			for (User user : userList) {
				UserDataDto userDataDto = UserPlatformUtil.generateUserDataFromUser(user);
				logger.debug("UserPlatformServiceImpl.getAllUsers:" + userDataDto.getUserName());
				userDataDtoList.add(userDataDto);
			}
		}else {
			throw new UserException(null,UserException.ErrorType.USERS_DOESNOT_EXISTS);
		}
		
		
		return userDataDtoList;
	}

	@Override
	public UserDataDto deactivateUserByRowId(String rowId) throws UserException {
		
		logger.debug("UserPlatformServiceImpl.deactivateUser: user data being deactivated:" + rowId);
		UserDataDto userDataDto = getUserByrowId(rowId);
		int userdeactivated = userDao.deactivateUserByRowId(rowId);
			if (userdeactivated < 1) {
				throw new UserException(userDataDto,UserException.ErrorType.USER_DEACTIVATION_FAILED);
			}
		return userDataDto;
		
	}
	
	@Override
	public int deactivateUsers() {
		
		logger.debug("UserPlatformServiceImpl.deactivateAllUsers");
		int totalUsers = userDao.deactivateAllUsers();
		return totalUsers;
		
	}

	@Override
	public UserDataDto updateUserByRowId(UserDataDto userDataDto, String rowId) throws UserException {
		
		logger.debug("UserPlatformServiceImpl.createUser: user data being updated:" + userDataDto);
			
		int updateUserStatus = userDao.updateUser(userDataDto.getUserName(), userDataDto.getPassword(), rowId);
		if (updateUserStatus < 1) {
			throw new UserException(userDataDto,UserException.ErrorType.USER_UPDATATION_FAILED);
		}
		
		return userDataDto;
		
	}

	@Override
	public UserDataDto reactivateUserByRowId(UserDataDto userDataDto, String rowId) throws UserException {
		
		logger.debug("UserPlatformServiceImpl.reactivateUser: user being activated:" + userDataDto);
			
		int reactivateUser = userDao.reactivateUser(rowId);
		if (reactivateUser < 1) {
			throw new UserException(userDataDto,UserException.ErrorType.USER_UPDATATION_FAILED);
		}
		
		return userDataDto;
		
	}

}
