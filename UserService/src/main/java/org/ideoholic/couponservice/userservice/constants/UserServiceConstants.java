package org.ideoholic.couponservice.userservice.constants;

public interface UserServiceConstants {

	String userServiceRESTName = "user";
	String SLASH = "/";
	String HYPHEN = "-";
	String VERSION = "v1";
	String PORT = "server.port";
	String DOT_STAR = ".*";
	String SERVICE_IDENTIFIER = "U";
	String USER_REST_CALL = SLASH + VERSION + SLASH + userServiceRESTName;

	int ROWID_LENGTH = 8;
}
