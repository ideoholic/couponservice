package org.ideoholic.couponservice.userservice;

import org.ideoholic.couponservice.userservice.constants.UserServiceConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig implements UserServiceConstants{

	@Bean
	public Docket postsApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("public-api").apiInfo(apiInfo()).select()
				.paths(postPaths()).build();
	}

	private Predicate<String> postPaths() {
		return regex(USER_REST_CALL + DOT_STAR);
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("CS API")
				.description("Copoun Service API reference for developers")
				.termsOfServiceUrl("http://ideoholic.com/").license("Ideoholic License")
				.licenseUrl("http://ideoholic.com/license").version("1.0").build();
	}

}
