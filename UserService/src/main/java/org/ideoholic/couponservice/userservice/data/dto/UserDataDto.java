package org.ideoholic.couponservice.userservice.data.dto;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Immutable data object for user data
 */
public class UserDataDto implements Comparable<UserDataDto>, Serializable {
	private static final long serialVersionUID = 5945110975432554671L;
	
	private final String rowId;
	private final String userName;
	private final String password;
	private boolean isActive;
	private Date registrationDate;
	private Date deactivationDate;
	private Date reactivationDate;

	public static UserDataDto template() {
		final String rowId = null;
		final String userName = null;
		final String password = null;
		final boolean isActive = true;
		final Date registrationDate = Calendar.getInstance().getTime();
		final Date deactivationDate = null;
		final Date reactivationDate = null;
		return new UserDataDto(rowId, userName, password, isActive, registrationDate, deactivationDate, reactivationDate);
	}

	public static UserDataDto withTemplate(final String userName, final String password) {
		final String rowId = null;
		final boolean isActive = true;
		final Date registrationDate = Calendar.getInstance().getTime();
		final Date deactivationDate = null;
		final Date reactivationDate = null;
		return new UserDataDto(rowId, userName, password, isActive, registrationDate, deactivationDate, reactivationDate);
	}

	public static UserDataDto instance(final String rowId, final String userName, final String password,
			final boolean isActive, final Date registrationDate, final  Date deactivationDate, final Date reactivationDate) {

		return new UserDataDto(rowId, userName, password, isActive, registrationDate, deactivationDate, reactivationDate);
	}

	public static UserDataDto lookup(final String rowId) {
		
		final String userName = null;
		final String password = null;
		final boolean isActive = true;
		final Date registrationDate = Calendar.getInstance().getTime();
		final Date deactivationDate = null;
		final Date reactivationDate = null;
		
		return new UserDataDto(rowId, userName, password, isActive, registrationDate, deactivationDate, reactivationDate);
	}

	protected UserDataDto(final String rowId, final String userName, final String password,
			final boolean isActive, final Date registrationDate, final Date deactivationDate, final Date reactivationDate) {
		this.rowId = rowId;
		this.userName = userName;
		this.password = password;
		this.isActive = isActive;
		this.registrationDate = registrationDate;
		this.deactivationDate = deactivationDate;
		this.reactivationDate = reactivationDate;
	}

	/**
	 * @return the rowId
	 */
	public final String getRowId() {
		return rowId;
	}

	public final String getUserName() {
		return userName;
	}

	public final String getPassword() {
		return password;
	}

	public final boolean isActive() {
		return isActive;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public Date getDeactivationDate() {
		return deactivationDate;
	}

	public Date getReactivationDate() {
		return reactivationDate;
	}

	@Override
	public boolean equals(final Object obj) {
		final UserDataDto userData = (UserDataDto) obj;
		return this.rowId.equals(userData.rowId);
	}

	@Override
	public int hashCode() {
		return this.rowId.hashCode();
	}

	@Override
	public int compareTo(final UserDataDto obj) {
		if (obj == null) {
			return -1;
		}
		return obj.rowId.compareTo(this.rowId);
	}

}