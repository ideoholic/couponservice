package org.ideoholic.couponservice.userservice.api;

import java.util.Collection;

import org.ideoholic.couponservice.exceptions.UserException;
import org.ideoholic.couponservice.userservice.constants.UserServiceConstants;
import org.ideoholic.couponservice.userservice.data.dto.UserDataDto;
import org.ideoholic.couponservice.userservice.service.UserPlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(UserServiceConstants.USER_REST_CALL)
public class UserServiceApiResource {

	@Autowired
	private final UserPlatformService userService;

	@Autowired
	public UserServiceApiResource(final UserPlatformService userService) {
		this.userService = userService;
	}

	@PostMapping("/")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<UserDataDto> createNewUser(@RequestBody final UserDataDto userDataDto) throws UserException {
		UserDataDto data = userService.validateAndSaveUser(userDataDto);
		return new ResponseEntity<UserDataDto>(data, HttpStatus.OK);
	}
	
	@GetMapping("/{rowId}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<UserDataDto> getUser(@PathVariable String rowId) throws UserException {
		UserDataDto userData = userService.getUserByrowId(rowId);
		return new ResponseEntity<UserDataDto>(userData, HttpStatus.OK);
	}
	
	@GetMapping("/")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Collection<UserDataDto>> getAllUsers() throws UserException {
		Collection<UserDataDto> userDataList = userService.getAllUsersData();
		return new ResponseEntity<Collection<UserDataDto>>(userDataList, HttpStatus.OK);
	}
	
	@PutMapping("/{rowId}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<UserDataDto> updateUser(@RequestBody final UserDataDto userDataDto, @PathVariable String rowId) throws UserException {
		UserDataDto userData = userService.updateUserByRowId(userDataDto, rowId);
		return new ResponseEntity<UserDataDto>(userData, HttpStatus.OK);
	}
	
	@DeleteMapping("/{rowId}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<UserDataDto> deactivateUser(@PathVariable String rowId) throws UserException {
		UserDataDto userData = userService.deactivateUserByRowId(rowId);
		return new ResponseEntity<UserDataDto>(userData, HttpStatus.OK);
	}
	
	@DeleteMapping("/deactivateall")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Integer> deactivateAllUsers() {
		int usersDeletion =	userService.deactivateUsers();
		return new ResponseEntity<Integer>(usersDeletion, HttpStatus.OK);
	}
	
	@PutMapping("/reacitvate/{rowId}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<UserDataDto> reactivateUser(@RequestBody final UserDataDto userDataDto, @PathVariable String rowId) throws UserException {
		UserDataDto userData = userService.reactivateUserByRowId(userDataDto, rowId);
		return new ResponseEntity<UserDataDto>(userData, HttpStatus.OK);
	}
}