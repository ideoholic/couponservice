
-- Disable foreign key check before dropping table
SET foreign_key_checks = 0;

-- drop related tables, if exists
DROP TABLE IF EXISTS cs_offers;

-- Enable foreign key check
SET foreign_key_checks = 1;


-- DDL for offer service related tables
CREATE TABLE `cs_offers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(20) NOT NULL,
  `store_id` int(11) NOT NULL,
  `offer_name` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `is_valid` TINYINT(1) NOT NULL DEFAULT '1',
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `createdby_id` bigint(20) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `lastmodifiedby_id` bigint(20) DEFAULT NULL,
  `lastmodified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT UC_offers_row_id UNIQUE(row_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--CONSTRAINT `FK_offers_in_store` FOREIGN KEY (`store_id`) REFERENCES `cs_store` (`id`),
--CONSTRAINT `FK_offers_createdby` FOREIGN KEY (`createdby_id`) REFERENCES `cs_user` (`id`),
--CONSTRAINT `FK_offers_lastmodifiedby` FOREIGN KEY (`lastmodifiedby_id`) REFERENCES `cs_user` (`id`)
