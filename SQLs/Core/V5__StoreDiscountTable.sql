
-- Disable foreign key check before dropping table
SET foreign_key_checks = 0;

-- drop related tables, if exists
DROP TABLE IF EXISTS st_discount;

-- Enable foreign key check
SET foreign_key_checks = 1;


-- DDL for store discount service related tables
CREATE TABLE `st_discount` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `store_name` varchar(100) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `offer_amount` bigdecimal NOT NULL,
  `customer_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;