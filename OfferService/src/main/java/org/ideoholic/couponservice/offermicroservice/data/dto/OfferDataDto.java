package org.ideoholic.couponservice.offermicroservice.data.dto;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Immutable data object for offer data
 */
//@JsonIgnoreProperties({ "id", "systemId" })
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OfferDataDto implements Comparable<OfferDataDto>, Serializable {
	private static final long serialVersionUID = 5945110975432554671L;

	// @JsonIgnore
	// @JacksonInject
	private final String offerRowId;
	private final String storeRowId;
	private final String offerName;
	private final String description;
	private boolean isValid;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date startDate;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date endDate;

	public static OfferDataDto template() {
		final String rowId = null;
		final String storeId = null;
		final String offerName = null;
		final String description = null;
		final boolean isValid = true;
		final Date startDate = Calendar.getInstance().getTime();
		final Date endDate = startDate;
		return new OfferDataDto(rowId, storeId, offerName, description, isValid, startDate, endDate);
	}

	public static OfferDataDto withTemplate(final String storeId, final String offerName, final String description) {
		final String rowId = null;
		final boolean isValid = true;
		final Date startDate = Calendar.getInstance().getTime();
		final Date endDate = startDate;
		return new OfferDataDto(rowId, storeId, offerName, description, isValid, startDate, endDate);
	}

	public static OfferDataDto instance(final String rowId, final String storeId, final String offerName,
			final String description, final boolean isValid, final Date startDate, final Date endDate) {

		return new OfferDataDto(rowId, storeId, offerName, description, isValid, startDate, endDate);
	}

	public static OfferDataDto lookup(final String rowId) {
		final String storeId = null;
		final String offerName = null;
		final String description = null;
		final boolean isValid = true;
		final Date startDate = Calendar.getInstance().getTime();
		final Date endDate = startDate;
		return new OfferDataDto(rowId, storeId, offerName, description, isValid, startDate, endDate);
	}

	protected OfferDataDto(final String rowId, final String storeId, final String offerName, final String description,
			final boolean isValid, final Date startDate, final Date endDate) {
		this.offerRowId = rowId;
		this.storeRowId = storeId;
		this.offerName = offerName;
		this.description = description;
		this.isValid = isValid;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public final String getOfferRowId() {
		return offerRowId;
	}

	public String getStoreRowId() {
		return storeRowId;
	}

	public String getOfferName() {
		return offerName;
	}

	public String getDescription() {
		return description;
	}

	public boolean isValid() {
		return isValid;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	@Override
	public boolean equals(final Object obj) {
		final OfferDataDto offerData = (OfferDataDto) obj;
		return this.offerRowId.equals(offerData.offerRowId);
	}

	@Override
	public int hashCode() {
		return this.offerRowId.hashCode();
	}

	@Override
	public int compareTo(final OfferDataDto obj) {
		if (obj == null) {
			return -1;
		}
		return obj.offerRowId.compareTo(this.offerRowId);
	}

}