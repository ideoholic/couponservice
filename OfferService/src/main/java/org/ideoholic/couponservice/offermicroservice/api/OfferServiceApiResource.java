package org.ideoholic.couponservice.offermicroservice.api;

import org.ideoholic.couponservice.offermicroservice.constants.OfferServiceConstants;
import org.ideoholic.couponservice.offermicroservice.data.dto.OfferDataDto;
import org.ideoholic.couponservice.offermicroservice.exceptions.OfferServiceException;
import org.ideoholic.couponservice.offermicroservice.service.OfferPlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(OfferServiceConstants.OFFERS_REST_CALL)
public class OfferServiceApiResource {

	private final OfferPlatformService offerService;

	@Autowired
	public OfferServiceApiResource(final OfferPlatformService offerService) {
		this.offerService = offerService;
	}

	@GetMapping("/")
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody ResponseEntity<String> getAllOffers() {
		return new ResponseEntity<String>("response", HttpStatus.OK);
	}

	@GetMapping("/{rowId}")
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody ResponseEntity<OfferDataDto> getGivenOfferByRowId(@PathVariable String rowId)
			throws OfferServiceException {
		OfferDataDto response = offerService.getOfferByRowId(rowId);
		return new ResponseEntity<OfferDataDto>(response, HttpStatus.OK);
	}

	@DeleteMapping("/{rowId}")
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody ResponseEntity<OfferDataDto> deleteGivenOfferByRowId(@PathVariable String rowId)
			throws OfferServiceException {
		OfferDataDto response = offerService.deleteOfferByRowId(rowId);
		return new ResponseEntity<OfferDataDto>(response, HttpStatus.OK);
	}

	@PostMapping("/")
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody ResponseEntity<OfferDataDto> creatNewOffer(@RequestBody final OfferDataDto offerData)
			throws OfferServiceException {
		OfferDataDto response = offerService.validateAndSaveOffer(offerData);
		return new ResponseEntity<OfferDataDto>(response, HttpStatus.OK);
	}

}