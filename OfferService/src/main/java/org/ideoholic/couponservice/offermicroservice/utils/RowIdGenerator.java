package org.ideoholic.couponservice.offermicroservice.utils;

import java.util.Random;

import org.ideoholic.couponservice.offermicroservice.constants.OfferServiceConstants;

public class RowIdGenerator implements OfferServiceConstants {

	public static String generateRowId(int length) {

		// ||, -, *, /, <>, <, >, ,(comma), =, <=, >=, ~=, !=, ^=, (, )
		int len = length / 2;
		String letters = new String("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
		String numbers = new String("1234567890");
		int lettersLength = letters.length();
		int numbersLength = numbers.length();

		StringBuffer generatedRandomString = new StringBuffer();

		for (int i = 0; i < len; i++) {
			int index = generateIndex(lettersLength);
			generatedRandomString.append(letters.charAt(index));
		}

		for (int i = 0; i < len; i++) {
			int index = generateIndex(numbersLength);
			generatedRandomString.append(numbers.charAt(index));
		}
		if (generatedRandomString.length() >= length) {
			generatedRandomString = generatedRandomString.delete(length - 1, generatedRandomString.length());
		}
		generatedRandomString = generatedRandomString.append(SERVICE_IDENTIFIER);
		return generatedRandomString.toString();

	}

	private static int generateIndex(int length) {
		Random random = new Random();
		int mathrandom = random.nextInt(length);

		do {
			double randomDouble = Math.random();
			if (randomDouble < 0.01)
				continue;
			mathrandom = (int) (mathrandom * randomDouble);
		} while (mathrandom >= length);
		return mathrandom;
	}

	public static void main(String[] args) {
		String rowId = generateRowId(10);
		System.out.println("Generated Row ID is:" + rowId);
	}

}
