package org.ideoholic.couponservice.offermicroservice.exceptions;

import org.ideoholic.couponservice.offermicroservice.data.dto.OfferDataDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public abstract class OfferServiceException extends Exception {
	public enum ErrorType {
		EXPIRED_OFFER, STARTDATE_AFTER_END_DATE, ENDDATE_AFTER_CURRENT_DATE, INVALID_STORE_ROWID, INVALID_OFFER_ROWID
	}

	private ErrorType type;
	private OfferDataDto offerDto;

	public OfferServiceException() {
		super();
	}

	public OfferServiceException(OfferDataDto offerDto, ErrorType type) {
		super();
		this.type = type;
		this.offerDto = offerDto;
	}

	public OfferServiceException(String message, OfferDataDto offerDto, ErrorType type) {
		super(message);
		this.type = type;
		this.offerDto = offerDto;
	}

	protected ErrorType getType() {
		return type;
	}

	protected OfferDataDto getOfferDto() {
		return offerDto;
	}

	@Override
	public abstract String toString();

}
