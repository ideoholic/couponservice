package org.ideoholic.couponservice.offermicroservice.constants;

public interface OfferServiceConstants {

	String offerServiceRESTName = "offers";
	String SLASH = "/";
	String HYPHEN = "-";
	String VERSION = "v1";
	String PORT = "server.port";
	String DOT_STAR = ".*";
	String SERVICE_IDENTIFIER = "O";
	String OFFERS_REST_CALL = SLASH + VERSION + SLASH + offerServiceRESTName;

	int ROWID_LENGTH = 9;
}
