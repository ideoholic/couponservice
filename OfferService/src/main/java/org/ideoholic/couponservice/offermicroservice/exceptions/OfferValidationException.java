package org.ideoholic.couponservice.offermicroservice.exceptions;

import org.ideoholic.couponservice.offermicroservice.data.dto.OfferDataDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class OfferValidationException extends OfferServiceException {

	public OfferValidationException(OfferDataDto offerDto, ErrorType type) {
		super(offerDto, type);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		switch (getType()) {
		case INVALID_OFFER_ROWID:
			sb.append("Invalid Offer Row ID:" + getOfferDto().getOfferRowId() + " passed");
			break;
		default:
			sb.append("General Offer Validation Exception");
			break;
		}
		return sb.toString();
	}

}
