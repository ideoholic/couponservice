package org.ideoholic.couponservice.offermicroservice.service;

import org.ideoholic.couponservice.offermicroservice.data.db.Offer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class OfferToStoreCommunicationServiceImpl implements OfferToStoreCommunicationService {

	private final static Logger logger = LoggerFactory.getLogger(OfferToStoreCommunicationServiceImpl.class);

	private final WebClient.Builder webClient;

	@Autowired
	OfferToStoreCommunicationServiceImpl(final WebClient.Builder webClient) {
		this.webClient = webClient;
	}

	@Override
	public Long getStoreIfFromRowId(String storeRowId) {
		logger.debug("OfferToStoreCommunicationServiceImpl.getStoreIfFromRowId:Passed storeRowId is:" + storeRowId);
		// Flux and Mono objects
		Offer off = webClient.build().get().uri("http://STORESERVICE/v1/getStoreByRowId/" + storeRowId).retrieve()
				.bodyToMono(Offer.class).block();
		return off.getId();
	}
}
