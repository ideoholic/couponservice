package org.ideoholic.couponservice.offermicroservice.data.dao;

import org.ideoholic.couponservice.offermicroservice.data.db.Offer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OfferDataDao extends JpaRepository<Offer, Long>, JpaSpecificationExecutor<Offer> {

	@Query("select offer from Offer offer where isValid = true and rowId = :rowId")
	Offer getOneByRowId(String rowId);

}
