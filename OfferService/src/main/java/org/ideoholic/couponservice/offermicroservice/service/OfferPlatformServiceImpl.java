package org.ideoholic.couponservice.offermicroservice.service;

import java.util.Date;

import org.ideoholic.couponservice.offermicroservice.data.dao.OfferDataDao;
import org.ideoholic.couponservice.offermicroservice.data.db.Offer;
import org.ideoholic.couponservice.offermicroservice.data.dto.OfferDataDto;
import org.ideoholic.couponservice.offermicroservice.exceptions.DateValidationException;
import org.ideoholic.couponservice.offermicroservice.exceptions.OfferServiceException;
import org.ideoholic.couponservice.offermicroservice.exceptions.OfferValidationException;
import org.ideoholic.couponservice.offermicroservice.exceptions.StoreValidationException;
import org.ideoholic.couponservice.offermicroservice.utils.OfferPlatformUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OfferPlatformServiceImpl implements OfferPlatformService {

	private final static Logger logger = LoggerFactory.getLogger(OfferPlatformServiceImpl.class);

	private final OfferDataDao offerDao;
	private final OfferToStoreCommunicationService storeCommService;

	@Autowired
	OfferPlatformServiceImpl(final OfferDataDao offerDao, final OfferToStoreCommunicationService storeCommService) {
		this.offerDao = offerDao;
		this.storeCommService = storeCommService;
	}

	@Override
	public OfferDataDto validateAndSaveOffer(OfferDataDto offerData) throws OfferServiceException {
		logger.debug("OfferPlatformServiceImpl.validateAndSaveOffer: Offer data being saved::" + offerData);

		Long storeId = validateOffer(offerData);

		Offer offer = OfferPlatformUtils.generateOfferFromOfferData(offerData, storeId);
		offerDao.saveAndFlush(offer);
		offerData = OfferPlatformUtils.generateOfferDataFromOffer(offer, offer.getRowId());
		return offerData;
	}

	private Long validateOffer(OfferDataDto offerData) throws OfferServiceException {
		validateOfferDates(offerData);
		return validateStoreId(offerData);
	}

	private void validateOfferDates(OfferDataDto offerData) throws DateValidationException {
		Date todayDate = OfferPlatformUtils.getCurrentDate();
		Date startDate = offerData.getStartDate();
		Date endDate = offerData.getEndDate();
		if (todayDate.after(endDate)) {
			throw new DateValidationException(offerData, DateValidationException.ErrorType.ENDDATE_AFTER_CURRENT_DATE);
		}
		if (startDate.after(endDate)) {
			throw new DateValidationException(offerData, DateValidationException.ErrorType.STARTDATE_AFTER_END_DATE);
		}
		if (!offerData.isValid()) {
			throw new DateValidationException(offerData, DateValidationException.ErrorType.EXPIRED_OFFER);
		}
	}

	private Long validateStoreId(OfferDataDto offerData) throws StoreValidationException {
		Long storeId = storeCommService.getStoreIfFromRowId(offerData.getStoreRowId());
		if (storeId == null) {
			throw new StoreValidationException(offerData, StoreValidationException.ErrorType.INVALID_STORE_ROWID);
		}
		return storeId;
	}

	@Override
	public OfferDataDto getOfferByRowId(String rowId) throws OfferServiceException {
		Offer offer = offerDao.getOneByRowId(rowId);
		OfferDataDto offerDto = OfferDataDto.lookup(rowId);
		if (offer == null) {
			throw new OfferValidationException(offerDto, OfferValidationException.ErrorType.INVALID_STORE_ROWID);
		}
		OfferDataDto offerData = OfferPlatformUtils.generateOfferDataFromOffer(offer, offer.getRowId());
		return offerData;
	}

	@Override
	public OfferDataDto deleteOfferByRowId(String rowId) throws OfferServiceException {
		OfferDataDto offerDto = OfferDataDto.lookup(rowId);
		Offer offer = offerDao.getOneByRowId(rowId);
		if (offer == null) {
			throw new OfferValidationException(offerDto, OfferValidationException.ErrorType.INVALID_STORE_ROWID);
		}
		OfferDataDto offerData = OfferPlatformUtils.generateOfferDataFromOffer(offer, offer.getRowId());
		offerDao.delete(offer);
		return offerData;
	}
}
