
package org.ideoholic.couponservice.offermicroservice.service;

public interface OfferToStoreCommunicationService {

	Long getStoreIfFromRowId(String storeRowId);

}