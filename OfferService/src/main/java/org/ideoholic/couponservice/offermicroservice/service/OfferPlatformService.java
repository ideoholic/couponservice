
package org.ideoholic.couponservice.offermicroservice.service;

import org.ideoholic.couponservice.offermicroservice.data.dto.OfferDataDto;
import org.ideoholic.couponservice.offermicroservice.exceptions.OfferServiceException;

public interface OfferPlatformService {

	OfferDataDto validateAndSaveOffer(OfferDataDto offerData) throws OfferServiceException;

	OfferDataDto getOfferByRowId(String rowId) throws OfferServiceException;

	OfferDataDto deleteOfferByRowId(String rowId) throws OfferServiceException;

}