package org.ideoholic.couponservice.offermicroservice.exceptions;

import org.ideoholic.couponservice.offermicroservice.data.dto.OfferDataDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DateValidationException extends OfferServiceException {

	public DateValidationException(OfferDataDto offerDto, ErrorType type) {
		super("Invalid date for the offer:" + offerDto.getOfferName(), offerDto, type);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(this.getMessage());
		switch (getType()) {
		case STARTDATE_AFTER_END_DATE:
			sb.append("- End date is after start date!");
			break;
		case ENDDATE_AFTER_CURRENT_DATE:
			sb.append("- End date is after current date!");
			break;
		default:
			break;
		}
		return sb.toString();
	}

}