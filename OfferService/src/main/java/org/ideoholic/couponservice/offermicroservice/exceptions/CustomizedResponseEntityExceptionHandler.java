package org.ideoholic.couponservice.offermicroservice.exceptions;

import static org.ideoholic.couponservice.offermicroservice.utils.OfferPlatformUtils.getCurrentDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	private final static Logger logger = LoggerFactory.getLogger(CustomizedResponseEntityExceptionHandler.class);

	@ExceptionHandler(OfferServiceException.class)
	public final ResponseEntity<ErrorDetails> handleOfferRelatedExceptions(OfferServiceException ex,
			WebRequest request) {
		logger.debug("CustomizedResponseEntityExceptionHandler::Exception message:" + ex.getMessage());
		// Set the error name in the header for the client to handle accordingly
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("ErrorType", ex.getType().name());

		ErrorDetails errorDetails = new ErrorDetails(getCurrentDate(), ex.toString(), request.getDescription(false));
		return ResponseEntity.badRequest().headers(responseHeaders).body(errorDetails);
	}
}
