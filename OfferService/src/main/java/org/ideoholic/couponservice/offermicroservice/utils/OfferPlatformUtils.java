package org.ideoholic.couponservice.offermicroservice.utils;

import java.util.Date;

import org.ideoholic.couponservice.offermicroservice.data.db.Offer;
import org.ideoholic.couponservice.offermicroservice.data.dto.OfferDataDto;

public class OfferPlatformUtils {

	public static Date getCurrentDate() {
		Date date = java.util.Calendar.getInstance().getTime();
		return date;
	}

	public static Offer generateOfferFromOfferData(OfferDataDto offerData, Long storeId) {
		Offer offer = new Offer();
		offer.setStoreId(storeId);
		offer.setOfferName(offerData.getOfferName());
		offer.setDescription(offerData.getDescription());
		offer.setValid(offerData.isValid());
		offer.setStartDate(offerData.getStartDate());
		offer.setEndDate(offerData.getEndDate());
		Date date = getCurrentDate();
		offer.setCreatedDate(date);
		return offer;
	}

	public static OfferDataDto generateOfferDataFromOffer(Offer offer, String storeRowId) {
		OfferDataDto data = OfferDataDto.instance(offer.getRowId(), storeRowId, offer.getOfferName(),
				offer.getDescription(), offer.isValid(), offer.getStartDate(), offer.getEndDate());
		return data;
	}

}
