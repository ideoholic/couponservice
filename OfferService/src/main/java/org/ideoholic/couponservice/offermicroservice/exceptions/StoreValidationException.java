package org.ideoholic.couponservice.offermicroservice.exceptions;

import org.ideoholic.couponservice.offermicroservice.data.dto.OfferDataDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class StoreValidationException extends OfferServiceException {

	public StoreValidationException(OfferDataDto offerDto, ErrorType type) {
		super(offerDto, type);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		switch (getType()) {
		case INVALID_STORE_ROWID:
			sb.append("Invalid Store Row ID:" + getOfferDto().getStoreRowId() + " passed");
			break;
		default:
			sb.append("General Store Validation Exception");
			break;
		}
		return sb.toString();
	}

}
